import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../login/authentication.service';
import {Constant} from '../common/constant';

@Component({
    moduleId: module.id,
    selector: 'forgot-password',
    templateUrl: 'forgotpassword.component.html'
})

export class ForgotPasswordComponent {

    constructor(
        private router: Router,
        private constant: Constant,
        private authService: AuthService
    ) {}

    showForgotPasswordScreen: boolean = true;
    showSendPasswordScreen: boolean = false;
    forgotPasswordEmail: string;
    error: string;
    isRequesting: boolean = false;

    forgotPasword() {
        this.showForgotPasswordScreen = false;
        this.showSendPasswordScreen = true;
        let ctrl = this;
        this.isRequesting = true;
        this.authService.forgotPassword(this.forgotPasswordEmail).then(function () {
        }).catch(function (error) {
            if (error && error._body) {
                ctrl.error = JSON.parse(error._body).error_description;
            }
            ctrl.isRequesting = false;
        });
    }
    onClickBackButton() {
        this.router.navigate([this.constant.loginUrl]);
    }
}
