/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Service: This service is used to handle suppressions
 * @Author: Shreya Shah
 */

import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import 'rxjs/add/operator/toPromise';
import {Suppression} from '../suppression/suppression';
import {Configuration} from '../services/configuration';
import {Common} from '../common/common';

@Injectable()
export class SuppressionService {

    private actionUrl: string;
    constructor(private http: HttpService,private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }

    get(): Promise<Suppression[]> {
        return this.http
            .get(this.actionUrl +'ered/suppressionrules')
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    getById(id: number): Promise<Suppression> {
        return this.http.get(this.actionUrl +"ered/suppressionrules/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    add(suppression: Suppression) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/suppressionrules", suppression)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    update(suppression: Suppression) : Promise<any> {
        return this.http.put(this.actionUrl +"ered/suppressionrules", suppression)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    publish(suppression: Suppression) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/publish/suppressionrules/", suppression)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    copy(id: number): Promise<any> {
        console.log("id :"+id)
        return this.http.put(this.actionUrl +"ered/copy/suppressionrules/"+id,"")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    archive(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/archive/suppressionrules/"+id,"")
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }

    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl +"ered/suppressionrules/"+id)
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }

    nextVersion(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/version/suppressionrules/"+id, "")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
}

