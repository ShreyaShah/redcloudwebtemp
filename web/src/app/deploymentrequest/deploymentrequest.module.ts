/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DeploymentRequestComponent} from './deploymentrequest.component';
import {DeploymentComponent} from './deployment.component';
import {PendingApprovalComponent} from './pendingapproval.component';
import {DeploymentRequestService} from './deploymentrequest.service';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule
    ],
    declarations: [
        DeploymentRequestComponent,DeploymentComponent,PendingApprovalComponent
    ],
    providers: [DeploymentRequestService]
})
export class DeploymentRequestModule {

}
