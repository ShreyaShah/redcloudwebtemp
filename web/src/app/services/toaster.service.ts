import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Message } from 'primeng/primeng';

@Injectable()
export class ToasterService {
    private subject = new Subject<any>();
    msgs: Message[] = [];
    sendToaster(serverity: string, summary: string, detail: string) {
        this.msgs.push({ severity: serverity, summary: summary, detail: detail });
        this.subject.next(this.msgs);
    }

    clearToaster() {
        this.subject.next();
    }
    
    getToaster(): Observable<any> {
        return this.subject.asObservable();
    }
}